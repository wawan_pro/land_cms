<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('user_agent');
	}

	public function access_control() {

		if (!$this->session->userdata('access')) return false;
		else return true;

	}

	public function login() {
		// достаем пароль из базы
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('name','pass');

		$query = $this->db->get();
		$row = $query->row();
		$db_pass = $row->value;
		$user_pass = $this->input->post('pass', TRUE);

		// проверяем пароль
		if($db_pass === md5($user_pass)) {
			$this->session->set_userdata('access', true);
			return true;
		} else 
			return false;
	}

}
