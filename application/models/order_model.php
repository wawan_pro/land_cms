<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model {

	protected $data = array();

	public function save($land_uri) {
		$this->data['name'] = $this->input->post('name', TRUE);
		$this->data['email'] = $this->input->post('email', TRUE);
		$this->data['phone'] = $this->input->post('phone', TRUE);
		
		$sess = $this->session->userdata($land_uri);

		$send = array(
               'landing_id' => $sess['land_id'],
               'variant_id' => $sess['variant'],
               'name' => $this->data['name'],
               'email' => $this->data['email'],
               'phone' => $this->data['phone'],
               'time' => time(),
               'status' => 1,
            );

		$this->db->insert('orders', $send);
		$last_id = $this->db->insert_id();

		// $this->send_email();
		return $last_id;
	}

	public function send_email() {
		$this->load->library('email');
		$this->load->helper('url');
		// готовим все данные
		$user_message = $this->load->view('user_mail', $this->data, true);
		$thx = $this->load->view('thx', $this->data, true);
		$adm_message = $this->load->view('admin_mail', $this->data, true);

		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('name','admin_mail');
		$query = $this->db->get();
		$row = $query->row();
		$admin_mail = $row->value;

		// отправляем
		$this->email->from($admin_mail, base_url());
		$this->email->to($this->data['email']); 
		$this->email->subject("Ваша заявка принята!");
		$this->email->message($user_message);	

		$this->email->send();
		
		$this->email->from('robot@site-optimize.ru' ,'Site-optimize.ru');
		$this->email->to($admin_mail); 
		$this->email->subject('Новый заказ!');
		$this->email->message($adm_message);	

		$this->email->send();
		
	}

	public function get_statuses() {
		$query = $this->db->get('statuses');
		$res = $query->result_array();
		return $res;
	}

	public function get_landings() {
		$query = $this->db->get('landings');
		$res = $query->result_array();
		return $res;
	}

	public function get_orders($status=0, $time, $land_id) {
		$this->db->select('orders.id, landing_id, name, email, phone, status_name');
		$where = array();
		if ($time>0) {
			$where["time > "] = "$time";
		}
		if ($status != 0) {
			$where["status"] = $status;
		}
		if ($land_id != 0) {
			$where["landing_id"] = $land_id;
		}
		if(!empty($where)) {
			$this->db->where($where);
		}
		$this->db->order_by("time", "asc");
		$this->db->join('statuses', 'statuses.id=orders.status');
		$query = $this->db->get('orders');
		$res = $query->result_array();

		return $res;
	}

	public function change_status($id,$status) {
		$this->db->where('id', $id);
		$this->db->set('status',$status);
		$query = $this->db->update('orders');
		
		return 1;
	}

}