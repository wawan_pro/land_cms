<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistic_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->helper('url');
	}

	public function save($land_id,$variant_id,$ip) {
		// записать данные о юзер агенте и реферале.
		// Открыть сессию, записать id реферала.
		$this->db->set('views', 'views+1', FALSE);
		$this->db->where('id',$variant_id);
		$this->db->update('variants');

		$data = array(
               'landing_id' => $land_id,
               'variant_id' => $variant_id,
               'time' => time(),
               'referrer' => $this->agent->referrer(),
               'user_agent' => $this->agent->agent_string(),
               'ip' => $ip
            );

		$this->db->insert('statistics', $data);
		return $this->db->insert_id();
	}

	public function goal($ref_id) {
		$this->db->set('goal', 1);
		$this->db->where('id',$ref_id);
		$this->db->update('statistics');
	}


}
