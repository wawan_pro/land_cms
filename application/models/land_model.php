<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Land_model extends CI_Model {

	protected $_land_id = 0;

	protected $_variants = array();

	protected $_selected_variant = 0;

	public function land_id() {
		return $this->_land_id;
	}
	
	public function variant_id() {
		return $this->_selected_variant;
	}

	public function get_land_variant($land_uri) {
		
		// получить id лендинга по uri или вернуть false
		if (!$this->get_land_id($land_uri)) return false;

		// получить список вариантов для этого лендинга
		return $this->select_variant();		
	}

	public function get_land_id($uri) {
		$this->db->select('id');
		$this->db->from('landings');

		// если нет uri, то выбираем ленд по-умолчанию
		if ($uri=="") {
			$this->db->where('is_default', 1);
		} else {
			$this->db->where('uri', $uri);
		}
		
		$query = $this->db->get();

		// если такого нет, то выходим
		if ($query->num_rows() == 0) return false;

		$row = $query->row();
		$this->_land_id = $row->id;
		return true;
	}

	public function select_variant() {

		if ($this->session->userdata($this->_land_id) != false)
			return $this->session->userdata($this->_land_id);

		$this->db->select('id, view_name');
		$this->db->from('variants');
		$this->db->where(array(
			'landing_id' => $this->_land_id,
			'active' => 1,
		));

		// выбираем вариант с наименьшим количеством просмотров
		$this->db->order_by('views', 'asc');
		$this->db->limit(1);

		$query = $this->db->get();
		
		$row = $query->row();

		// запоминаем вбранный вариант для статистики
		$this->_selected_variant = $row->id;
		return $row->view_name;
	}

	public function get_variants() {
		return false;
	}

}