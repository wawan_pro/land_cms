<html>
    <head>
        <title>Заявка принята!</title>
    </head>
    <body>
        <p>Здравствуйте, <?=$name?>!</p>
        <p>Спасибо, что оставили заявку на нашем сайте Site-Optimize.ru. Наш менеджер свяжется с вами в ближайшее время!</p>
        <p>Если у вас есть какие-то вопросы, можете написать нам по адресу <a href="mailto:info@site-optimize.ru">info@site-optimize.ru</a> или просто ответить на это письмо</p>
        <p>С уважением, команда Site-Optimize.ru</p>
    </body>
</html>