		
		<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        	<h4>Статус</h4>
          <div class="list-group">
          	<a href="<?=site_url("admin/orders/index/0/$time/$land_id");?>" 
          		class="list-group-item <?=0==$sort_status?'active':''?>">Все</a>
			<? foreach($status as $row): ?>
				<a href="<?=site_url("admin/orders/index/{$row['id']}/$time/$land_id");?>" 
					class="list-group-item <?=$row['id']==$sort_status?'active':''?>">
					<?=$row['status_name'];?>
				</a>
			<? endforeach; ?>
          </div>
        	<h4>Лендинг</h4>
          <div class="list-group">
          	<a href="<?=site_url("admin/orders/index/$sort_status/$time/0");?>" 
          		class="list-group-item <?=0==$land_id?'active':''?>">Все</a>
			<? foreach($lands as $row): ?>
			<? if ($row['id']==$land_id) $land_name=$row['title']; ?>
				<a href="<?=site_url("admin/orders/index/0/$time/{$row['id']}");?>" 
					class="list-group-item <?=$row['id']==$land_id?'active':''?>">
					<?=$row['title'];?>
				</a>
			<? endforeach; ?>
          </div>
        </div><!--/span-->
        <div class="col-xs-12 col-sm-9">
          <div class="">
            <h1>Последние заявки
			<? if(isset($land_name) && !empty($land_name)) : ?>
				<small>(для лендинга "<?=$land_name?>")</small>
			<? else: ?>
				<small>(для всех страниц)</small>
			<?endif;?>
			</h1>
			<? if(count($stat) == 0) {?>	
				<div class="alert alert-info">Нет заявок с такими параметрами</div>
			<? } else {?>
			<table class="table">
			    <thead>
			        <tr>
			            <th>#</th>
			            <th>Имя</th>
			            <th>Email</th>
			            <th>Телефон</th>
			            <th>Статус</th>
			            <?if(!isset($land_name)||empty($land_name)):?>
			            <th>Landing</th>
			            <?endif;?>
			        </tr>
			    </thead>
			    
			    <tbody>
			        <? foreach($stat as $row): ?>
			            <tr>
			                <td><?=$row['id'];?></td>
			                <td><?=$row['name']?></td>
			                <td><?=htmlspecialchars($row['email'])?></td>
			                <td><?=htmlspecialchars($row['phone'])?></td>
			                <td>
			                	<form action="<?=site_url("admin/orders/edit");?>" method="post" class="form-inline" role="form">
			                		<div class="form-group">
				                	<select name="select" class="form-control">
				                	<? foreach($status as $st): ?>
					                	<option value="<?=$st['id'];?>" 
					                		<?=$st['status_name']==$row['status_name']?'selected':''?> >
											<?=$st['status_name'];?>
					                	</option>
									<? endforeach; ?>
									</select>
									</div>
									<input type="hidden" value="<?=$row['id'];?>" name="id" >	
									<button type="submit" class="btn btn-default">
										<span class="glyphicon glyphicon-pencil"></span>
									</button>
			                	</form>
			                </td>
			            	<?if(!isset($land_name)||empty($land_name)):?>
			                <td><?=$row['landing_id']?></td>
			                <?endif;?>
			            </tr>
			        <? endforeach; ?>
			    </tbody>
			</table>
			<?}?>
          </div>
        </div><!--/span-->
