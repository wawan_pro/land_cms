<html>
	<head>
		<title>
			Создание Landing Pages - Site-Optimize.ru
		</title>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif+Caption&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/public/so/css/style.css">

		<meta charset="utf-8" />
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter21758293 = new Ya.Metrika({id:21758293,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21758293" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	</head>
	<body>
		<div class="container">
			<div class="header">
				<p align="center">
					<a href="/"><img src="/public/so/img/header.png" alt="Site-Optimize.ru" ></a> 
				</p>
			</div>
			
			<div class="main">
                <div class="form">
                    <form action="/order/default" method="post" onsubmit="yaCounter21758293.reachGoal('ORDER'); return true;">
                        <p>
                            <input type="text" name="name">
                        </p>
                        <p>
                            <input type="text" name="email">
                        </p>
                        <p>                         
                            <input type="text" name="phone">
                        </p>
                        <p>
                            <input type="submit" value="">
                        </p>
                    </form>
                </div>
                <div class="offer">
                    <h1>Создание Landing Pages</h1>
                    <ul>
                        <li><p>По триггерам Бизнес Молодости!</p></li>
                        <li><p>Под ключ!</p></li>
                        <li><p>CMS с подробной аналитикой!</p></li>
                        <li><p>Разработка от 1 дня!</p></li>
                        <li><p>Гарантия конверсии от 5%</p></li>
                    </ul>
                </div>
                <div class="clear">&nbsp;</div>
            </div><div class="clear">&nbsp;</div>
            <div class="why">
                <img src="/public/so/img/why.png" alt="">
            </div>			
		</div>

		<div id="footer">
			<p>&copy; Site-Optimize.ru</p>
		</div>
	</body>
</html>