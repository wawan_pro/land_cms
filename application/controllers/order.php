<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('statistic_model');
		$this->load->model('order_model');
	}
	
	public function index($land_uri) {
		// если нет ref_id, то что-то тут нечисто...
		$ref_id = $this->session->userdata('ref_id');
		if (empty($ref_id)) {
			show_error('Попробуйте еще раз!');
			die();
		}

		// отправить зданные в модель Order
		$this->order_model->save($land_uri);

		// записать в статистику что был сделан заказ
		$this->statistic_model->goal($ref_id);

		// загрузить вьюху "спасибо!"
		$this->load->view('thx');
	}
}