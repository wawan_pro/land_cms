<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistics extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admin/user_model');
		$this->load->model('statistic_model');
	}

	public function index() {
		$this->output->enable_profiler(true);
		if (!$this->user_model->access_control())  {			
			$this->load->view('admin/signin');
		} else {
			$this->load->view('admin/header');

			echo "<h1>This is Statistics</h1>";

			$this->load->view('admin/footer');
		}
	}

}