<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admin/user_model');
		$this->load->model('statistic_model');
		$this->load->model('order_model');
		
		// проверяем логин админа
		if (!$this->user_model->access_control())
			redirect('/user/login', 'refresh');
	}
	
	public function index($sort_status=1, $time=0, $land_id = 0) {
		
		$this->load->view('admin/header');

		$data['land_id'] = $land_id;
		$data['time'] = $time;
		$data['sort_status'] = $sort_status;	
		$data['status'] = $this->order_model->get_statuses();
		$data['lands'] = $this->order_model->get_landings();
		$data['stat'] = $this->order_model->get_orders($sort_status, $time, $land_id);
		$this->load->view('admin/orders', $data);
		$this->load->view('admin/footer');
	}

	public function edit() {
		$status = $this->input->post('select',TRUE);
		$id = $this->input->post('id',TRUE);
		$this->order_model->change_status($id,$status);
		redirect($this->agent->referrer(), 'refresh');
	}

}