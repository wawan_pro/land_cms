<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Land extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('land_model');
		$this->load->model('statistic_model');
	}

	public function index($land_uri="default")
	{
		$this->output->enable_profiler(TRUE);

		if ($land_uri=='clear') {
			$this->session->sess_destroy();
			echo '<a href="/">main</a><br>';
			echo '<a href="/admin/">admin</a>';
			die();
		}

		if(!$this->session->userdata($land_uri)) {
			// Получить имя вьюхи
			$view_name = $this->land_model->get_land_variant($land_uri);
				
			// сохранить имя вьюхи, чтобы не лезть больше в базу и не учитывать в статистике.
			$sess = array(
				'view_name' => $view_name,
				'variant' => $this->land_model->variant_id(),
				'land_id' => $this->land_model->land_id()
			);

			$this->session->set_userdata($land_uri, $sess);
			// Записать статистику
			$ref_id = $this->statistic_model->save(
				$this->land_model->land_id(),
				$this->land_model->variant_id(),
				$this->session->userdata('ip_address')
			);
			// если юзер оставит заявку, нам нужен будет ref_id
			$this->session->set_userdata('ref_id', $ref_id);
		} else {
			$view_name = $this->session->userdata($land_uri);
			$view_name = $view_name['view_name'];
		}

		// Загрузить вьюху
		$this->load->view($view_name);
	}

	public function clear() {
		$this->session->sess_destroy();
		echo '<a href="/">main</a>';
	}
}
