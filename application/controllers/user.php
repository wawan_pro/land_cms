<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admin/user_model');
		$this->load->model('statistic_model');
		$this->load->model('order_model');
		
	}

	public function login() {
		if($this->user_model->login()) {
			redirect('admin/orders/index', 'refresh');
		} else {
			$this->load->view('admin/signin');
		}
	}

}
	